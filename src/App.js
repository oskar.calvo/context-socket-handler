import React, { useEffect, useState } from 'react'
import {SocketContext, socket} from './context/socketContext';
import Home from './components/Home';
import SocketListener from './components/SocketListener';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {

	const [globalState, setGlobalState] = useState({stamina: 45, life: 70, gold: 29, xp: 635});
	const [currentSocketEvent, setCurrentSocketEvent] = useState(null);

	const handleGlobalState = (data) => {
		setGlobalState(globalState => ({
			...globalState,
			...data
		}));
	}

	const handleCurrentEvent = (data) => {
		setCurrentSocketEvent(data);
	}
	
	useEffect(() => {         
		socket.onAny((event, ...args) => handleCurrentEvent({event, value: args[0]})); 
		return () => {
			socket.removeAllListeners();  
		};
	}, []);

	return (
		<SocketContext.Provider value={{globalState, handleGlobalState}}>
			<div className="App">
				<Home />
				{!currentSocketEvent ? null : <SocketListener currentSocketEvent={currentSocketEvent} />}			
			</div>
		</SocketContext.Provider>
	);
}

export default App;