import React, { useContext, useEffect } from 'react';
import { SocketContext } from '../context/socketContext';
import ProgressBar from 'react-bootstrap/ProgressBar';

const Home = () => {
    const context = useContext(SocketContext).globalState;
       
    return (
        <div style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <p>Home</p>
            <p>{context.user}</p>
            <p>GOLD</p>
            <ProgressBar now={context.gold} label={`${context.gold} coins`} />
            <p>STAMINA</p>
            <ProgressBar now={context.stamina} label={`${context.stamina}%`} />
            <p>LIFE</p>
            <ProgressBar now={context.life} label={`${context.life}%`} />
            <p>EXPERIENCE</p>
            <ProgressBar max={1500} now={context.xp} label={`${context.xp} points`} />
        </div>
    )
}

export default Home